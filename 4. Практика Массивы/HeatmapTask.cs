﻿using System;

namespace Names
{
    internal static class HeatmapTask
    {
        public static HeatmapData GetBirthsPerDateHeatmap(NameData[] names)
        {
            string[] days = new string[] {"2","3","4","5","6","7","8","9","10","11",
                    "12","13","14","15","16","17","18","19","20","21",
                    "22","23","24","25","26","27","28","29","30","31" };
            string[] months = new string[] { "1", "2", "3", "4", "5", "6", "7", "8",
                "9", "10", "11", "12", };
            double[,] map = new double[30,12];

            var daysCounts = new double[days.Length];
            var monthCounts = new double[months.Length];

                foreach (var name in names)
                {
                    if (name.BirthDate.Day != 1) map[name.BirthDate.Day - 2, name.BirthDate.Month - 1]++;
                }
            
                
            return new HeatmapData(
                "Пример карты интенсивностей",
                map, days, months);        
        }
    }
}