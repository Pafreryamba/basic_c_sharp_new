﻿using System;

namespace DistanceTask
{
	public static class DistanceTask
	{
		// Расстояние от точки (x, y) до отрезка AB с координатами A(ax, ay), B(bx, by)
		public static double GetDistanceToSegment(double ax, double ay, double bx, double by, double x, double y)
		{
			double a = Dlina(x, ax, y, ay);
			double b = Dlina(x, bx, y, by);
			double c = Dlina(ax, bx, ay, by);
			double pp = PoluPer(ax, ay, bx, by, x, y);
			if (Math.Pow(a, 2) > (Math.Pow(b, 2) + Math.Pow(c, 2)) ||
				Math.Pow(b, 2) > (Math.Pow(a, 2) + Math.Pow(c, 2)))
				return (Math.Min(a, b));
			else if (a + b == c) return 0.0;
			else if (c == 0) return a;
			else return (2 * Math.Sqrt(pp * (pp - a) * (pp - b) * (pp - c)) / c);
		}

		public static double Dlina(double x11, double x12, double y11, double y12)
        {
			return Math.Sqrt(Math.Pow((x11 - x12), 2) + Math.Pow((y11 - y12), 2));
        }
		public static double PoluPer(double ax1, double ay1, double bx1, double by1, double x1, double y1)
        {
			return (Dlina(ax1, bx1, ay1, by1) +
				Dlina(x1, bx1, y1, by1) +
				Dlina(ax1, x1, ay1, y1)) / 2;
		}
	}
}