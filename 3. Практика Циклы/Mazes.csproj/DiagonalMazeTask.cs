﻿using System;

namespace Mazes
{
	public static class DiagonalMazeTask
	{
		public static void MoveOut(Robot robot, int width, int height)
		{
			while (!robot.Finished)
			{
				if (height < width)
				{
					MoveRightMax(robot, width, height);
					if (robot.Finished) break;
					robot.MoveTo(Direction.Down);
				}
				else
				{
					MoveDownMax(robot, width, height);
					if (robot.Finished) break;
					robot.MoveTo(Direction.Right);
				}
			}
		}
		
		private static void MoveRightMax(Robot robot, int width, int height)
		{
			for (int i = 0; i < Math.Round((double)width / height); i++) robot.MoveTo(Direction.Right);
		}
		private static void MoveDownMax(Robot robot, int width, int height)
		{
			for (int i = 0; i < Math.Round((double)height / width); i++) robot.MoveTo(Direction.Down);
		}
	}
}